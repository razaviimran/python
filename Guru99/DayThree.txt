Hi again, 
 
First, let me say congrats you made it to day 3. Nothing in life can be learned with out taking the first step.
 
Today's lesson you will learn interesting Python stuff Enjoy!
 
--Lesson 9
Python Operators: Arithmetic, Logical, Comparison, Assignment, Bitwise & Precedence
https://www.guru99.com/python-operators-complete-tutorial.html
 
--Lesson 10
Python Functions Examples: Call, Indentation, Arguments & Return Values
https://www.guru99.com/functions-in-python.html
 
--Lesson 11
Python IF, ELSE, ELIF, Nested IF & Switch Case Statement
https://www.guru99.com/if-loop-python-conditional-structures.html
 
--Lesson 12
Python For & While Loops: Enumerate, Break, Continue Statement
https://www.guru99.com/python-loops-while-for-break-continue-enumerate.html

After you read today's lessons, practice! The only way you going to get understanding is  learning by doing.
 
Take Care, look out for tomorrow's lesson in your inbox.
 
Warmest Wishes,
Guru99 Team
 
Today's Quote:
"You miss 100% of the shots you don�t take."
Wayne Gretzky