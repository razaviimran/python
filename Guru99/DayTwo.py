#Variables: Declare, Concatenate, Global & Local

h = ("It is my variable ") #declare

g = 99 # another variable

print(h + str(g)) #concatenate string with int

i = ("I am global variable") # Global variable

def main():
	j = ("I am local variable")
	print(i) # lets print global variable here
 
main()
#print(j) #Local variable can not print after function

###
#Strings: Replace, Join, Split, Reverse, Uppercase & Lowercase

a = ("Hello ")

b = ("WOrld")

#string replace

oldstring = ("I like Guru99")
newstring = oldstring.replace('like', 'love')
print(newstring)

#upper & lower case
string = ("learning python at guru99")
print(string.upper()) #for all capital letter
print(string.capitalize())

upstring = ("LEARNING PYTHON AT GURU99")
print(upstring.lower())


#join function

