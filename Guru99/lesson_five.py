# Declare a variable
f = 20 
print(f)


# Re-declare 
f = "I am talking with Ansaf"
print(f)


# Delete the variable
del(f)
#print(f)

# Concatenate 2 variable
i = ("Guru")
j = 99

print(i + str(j))

# Global variable
g = "I am global variable, i am globally available"
print(g)


# Local variable
def myfunc():
	l = "I am local varible of myfunc function"
	print(l)

myfunc()
print(g)

