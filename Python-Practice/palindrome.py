anything = input('Enter anything you want to check :')

#One Way
reverse_input = anything[::-1]

if anything == reverse_input:
	print('Your input is palindrome')
else:
	print('Your input is not a palindrome')

# second way

for i in anything:
	check = ""
	