import sys, time, datetime, csv
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


timeStamp = str(int(time.time()))

print ("Google Search v1.0\n")

print (timeStamp)

search_query = raw_input("enter your search term to search on google:\n")

page = "https://google.com/"

driver = webdriver.Chrome()

wait = WebDriverWait(driver, 10)

driver.maximize_window()

actions = ActionChains(driver)

driver.get(page)

try:
    wait.until(EC.presence_of_element_located((By.XPATH, "//input[contains(@name,'q')]"))) 
    search_input = driver.find_element_by_xpath("//input[contains(@name,'q')]")
    search_input.click()
    search_input.clear()
    search_input.send_keys( search_query )
    search_input.send_keys( Keys.ENTER )
except Exception as e:
    raise e

search_query = raw_input("press enter to exit:\n")
driver.close()
driver.quit()