import sys, time, datetime, csv
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

# note: for above imports you might get errors that no module selenium found etc..
# to fix this error you need to install required modules using: pip install selenium
# like that install other dependencies using pip install

# capture current time of your system in milliseconds
timeStamp = str(int(time.time()))

# print your scrapper information
print ("Google Search v1.0\n")

# print the timestamp
print (timeStamp)

#take search query from user
search_query = raw_input("enter your search term to search on google:\n")

# define 1st page to open using web driver
page = "https://google.com/"

# grab webdriver object for chrome browser
driver = webdriver.Chrome()

# initilize minimum wait period for web driver object to wait upto 10 sec for any elements we want to capture
# so that web driver will wait 10 seconds for required elements using xpath(u can use by css, id, classname etc)
wait = WebDriverWait(driver, 10)

# maximize the browser window
driver.maximize_window()

# initialize action for web driver object to perform actions like click. mouse move etc.
actions = ActionChains(driver)

# load the page url in browser
driver.get(page)

try:
    # wait until the search input loaded fully.
    wait.until(EC.presence_of_element_located((By.XPATH, "//input[contains(@name,'q')]"))) 

    # get the search input object reference in search_input variable to manipulate human actions on it.
    search_input = driver.find_element_by_xpath("//input[contains(@name,'q')]")

    # click to activate search input field.
    search_input.click()

    # clear any text content already present in search input.
    search_input.clear()

    # type your search query using send_keys method.
    search_input.send_keys( search_query )

    # now press enter on search input text area to search google
    search_input.send_keys( Keys.ENTER )

except Exception as e:
    # print exceptions of any.
    raise e

# wait for user to press enter to exit the app, else this scrapper will open google put search and submit and will close
# so you can't see the browser anymore, to avoid this take user input so that app will wait for user input and will not close
# you can comment this line and check the difference
search_query = raw_input("press enter to exit:\n")

#close driver connections etc.
driver.close()

#quit web browser
driver.quit()