## keywords
# keywords are the reserverd words
# We cannot use a keyword as a variable name, function name or any other identifier. They are used to define the syntax and structure of the Python language.
# there are 33 keywords in Python 3.7

## Identifier
# An identifier is a name given to entities like class, functions, variables
# identifier cannot start with a digit
# cannot use special symbols like !, @, #, $, %
