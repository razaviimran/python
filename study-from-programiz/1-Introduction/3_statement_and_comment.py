## Python Statement, Indentation and Comments
# Multiline Statement

a = (1 + 2 + 3 +
	4 + 5 + 6 +
	7 + 8 + 9)
print(a)

colors = ['red',
			'blue',
			'green']
print(colors)

x = 1; y = 2; z = 3
print(x + y + z)

## Python Indentation
for i in range(1, 11):
	print(i)
	if i == 5:
		break


## Python Comment