### Python Variables, Constants and Literals
"""
Variables
"""


"""
Constants - it is a type of variable whose value can not be changed
make a file, named constant.py
"""
import constant

print(constant.PI)
print(constant.GRAVITY)
# always use capital letters where possible to declare a constant
# PI
# G
# MASS
# TEMP


"""
Literals - it is raw data given in a variable
"""
# Numeric Literals - Numeric Literals are immutable (unchangeable). Numeric literals can belong to 3 different numerical types Integer, Float and Complex.
a = 0b1010 #Binary Literals
b = 100 #Decimal Literals
c = 0o310 #Octal Literals
d = 0x12c #Hexadecimal Literals

#Float Literals
float_1 = 10.5
float_2 = 1.5e2

#Complex Literals
x = 3.14j

print(a, b, c, d)
print(float_1, float_2)
print(x, x.imag, x.real) 

##String Literals
string = "This is Python"
char = "C"
maltiline_str = """this is multi line string we can write any amount of data"""

unicode = "\u00dcnic\u00f6de"

raw_str = "raw \n string"

print(unicode)
print(raw_str)

## Boolean Literals
x = (1 == True)
y = (1 == False)
a = True + 4
b = False + 10

print("X is ", x)
print("Y is ", y)
print("a : ", a)
print("b : ", b)