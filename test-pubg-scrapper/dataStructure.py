## 4 Common Data Structures

#1) Arrays - A collection of element identified by an index or key
ex_arr = [1, 'string', 3, 'four']
print(ex_arr)
print(ex_arr[3])

#2) Linked Lists - 
#class Node(object):

#3) Stacks and Queues - 
# create a empty stack
stack = []
#push items onto the stack
stack.append(1)
stack.append(2)
stack.append(3)
stack.append(4)

#print the stack contents
print(stack) #Output - [1, 2, 3, 4]

#pop an item off the stack
x = stack.pop()

print(stack)
print("Pop item is", x) #concatenation

#4) Hash Tables (Dictionary)
