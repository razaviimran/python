import csv, sys, time, html
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

headers = ['profile_link']

print ("\nMeetUp v1.0, ( Sū fēi ān )\n")
filename = input("enter file name to save data:\n")

with open(str(filename) + '.csv', 'a', encoding='utf-8') as f:
	writer = csv.writer(f)
	writer.writerow(headers)

def cItems(key, array__data):
    if key in array__data:
        return array__data[key]
    else:
        return ''

def wait__for__ajax(driver):
    wait = WebDriverWait(driver, 20)
    try:
        wait.until(lambda driver: driver.execute_script('return jQuery.active') == 0)
        wait.until(lambda driver: driver.execute_script('return document.readyState') == 'complete')
    except Exception as e:
        pass

driver = webdriver.Chrome()
wait = WebDriverWait(driver, 10)
driver.maximize_window()
driver.get( "https://facebook.com/" )

time.sleep( 3 )

wait.until(EC.presence_of_element_located((By.XPATH, "//input[@name='email']")))
email__input = driver.find_element_by_xpath("//input[@name='email']")
email__input.clear()
email__input.send_keys("razaviimran@gmail.com")

wait.until(EC.presence_of_element_located((By.XPATH, "//input[@name='pass']")))
password__input = driver.find_element_by_xpath("//input[@name='pass']")
password__input.clear()
password__input.send_keys("123@#Imran")

wait.until(EC.presence_of_element_located((By.XPATH, "//label[@id='loginbutton']//input")))
button__input = driver.find_element_by_xpath("//label[@id='loginbutton']//input")
button__input.click()

time.sleep( 2 )
# temporary = input("navigate to the result page and press enter to START:\n")

# wait.until(EC.presence_of_element_located((By.XPATH, "//input[@name='q']")))
# search__input = driver.find_element_by_xpath("//input[@name='q']")
# search__input.clear()
# search__input.send_keys("pubg")
# search__input.send_keys(Keys.RETURN)

# wait.until(EC.presence_of_element_located((By.XPATH, '//li[@data-edge="keywords_blended_posts"]')))
# post__button = driver.find_element_by_xpath('//li[@data-edge="keywords_blended_posts"]')
# post__button.click()

# time.sleep( 2 )

# # driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

# wait.until(EC.presence_of_element_located((By.XPATH, "//a[contains(text(),'See All') and contains(@href,'stories-keyword')]")))
# people_link = driver.find_element_by_xpath("//a[contains(text(),'See All') and contains(@href,'stories-keyword')]")
# people_link.click()


temp = input("press enter when likes popup is opened....")

# //ul[contains(@id,'reaction_profile_browser')]//li//a[contains(@data-hovercard-prefer-more-content-show,'1')]
wait.until(EC.presence_of_element_located((By.XPATH,"//ul[contains(@id,'reaction_profile_browser')]//li//a[contains(@data-hovercard-prefer-more-content-show,'1')]")))
likes__comments = driver.find_elements_by_xpath("//ul[contains(@id,'reaction_profile_browser')]//li//a[contains(@data-hovercard-prefer-more-content-show,'1')]")

all__links = []
for likes in likes__comments:
	a__link = likes.get_attribute('href').strip()
	all__links.append(a__link)

for links in all__links:
	with open(str(filename) + '.csv', 'a', encoding='utf-8') as f:
		writer = csv.writer(f)
		writer.writerow([links])
	time.sleep( 1 )

temp = input("press enter to exit...")

driver.close()
driver.quit()




